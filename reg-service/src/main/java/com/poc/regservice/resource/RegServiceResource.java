package com.poc.regservice.resource;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poc.regservice.model.User;
import com.poc.regservice.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class RegServiceResource{
	private UserRepository userRepository;
	
		
	@Value("Welcome ")
	private String welcomeMessage;
	

    public RegServiceResource(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @GetMapping("/{name}")
    public List<String> getUsersByName(@PathVariable("name") final String name) {
    	System.out.println("reg-service.getUsersByName called "+name);
        return getUserByName(name );
    }
    
    @GetMapping
    public List<User> getUsers() {
    	System.out.println("db-service.getRegloyees called ");
        return userRepository.findAll();
    }

    @PostMapping("/register")
    public String add(@RequestBody final User user) {
    	user.setPassword(user.getName()+123);
    	User savedUser = userRepository.save(user);
        return savedUser.getName()+" registered successfully with user name "+ savedUser.getLoginid()
        + " and password="+savedUser.getPassword();
    }
    
    @PostMapping("/fund/transfer/{loginid1}/{loginid2}/{amount}")
    public String transferFund(@PathVariable("loginid1") String userid1,@PathVariable("loginid2")  String userid2, @PathVariable("amount") String amount) {
    	System.out.println("reg-service.transferFund called ");
    	
    	 userRepository.transferFund(userid1, userid2, amount);
    	return "fund transferred successfully";
    }
    
    
    @PostMapping("/login")
    private List<User> loginUser(@RequestBody final User user) {
    	return userRepository.findByLoginidAndPassword( user.getLoginid(),user.getPassword());
    }
    
    private List<String> getUserByName(@PathVariable("name") String username) {
        return userRepository.findByName(username)
                .stream()
                .map(User::getName)
                .map( a-> welcomeMessage+ a)
                .collect(Collectors.toList());
    }

   


}
