package com.poc.regservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.poc.regservice.repository")
@SpringBootApplication
public class RegServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegServiceApplication.class, args);
	}
}
