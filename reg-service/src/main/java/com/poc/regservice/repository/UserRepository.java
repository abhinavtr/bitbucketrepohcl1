package com.poc.regservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.poc.regservice.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, UserRepositoryCustom {
	List<User> findByName(String name);

	List<User> findAll();

	User getUserById(int id);

	public List<User> findByLoginidAndPassword(String username, String password);
}