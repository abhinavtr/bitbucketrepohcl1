package com.poc.regservice.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.poc.regservice.model.User;

@Repository
@Transactional(readOnly=false)
public class UserRepositoryImpl implements UserRepositoryCustom{ 
	
	@PersistenceContext
    EntityManager entityManager;

	

	@Override
	@Modifying
	public Integer transferFund(String user1, String user2, String amount) {
		Query query = entityManager.createNativeQuery("SELECT * FROM User " +
                "WHERE loginid=?", User.class);
        query.setParameter(1, user1 );
        User from = (User) query.getSingleResult();
        
        
        query = entityManager.createNativeQuery("SELECT user.* FROM User as user " +
                "WHERE user.loginid=?", User.class);
        query.setParameter(1, user2 );
        User to = (User) query.getSingleResult();
        int money  = Integer.parseInt(amount);
        if(from !=null && to !=null ) {
	        if(from.getBalance() > money) {
	        	from.setBalance(from.getBalance() - money);
	        	to.setBalance(to.getBalance() + money);
	        }
			
			query = entityManager.createNativeQuery("update User  set  balance=" + from.getBalance() +
	                " WHERE user.id=?", User.class);
	        query.setParameter(1, user1);
	        int count1 = query.executeUpdate();
	        
	        int count2 = 0;
	        if(count1>0){
	        query = entityManager.createNativeQuery("update User  set  balance=" + to.getBalance() +
	                " WHERE user.id=?", User.class);
	        query.setParameter(1, user2);
	        count2 = query.executeUpdate();
	        }
	        if(count1>0 && count2>0) {
	        	return count1;
	        }
        }
        
        return 0;
        
	}

	
	

}
